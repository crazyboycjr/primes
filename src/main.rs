use primes::primes;
fn main() {
    let p = primes().take(10000).last();
    println!("primes: {:?}", p);
}
