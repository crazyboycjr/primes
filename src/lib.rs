#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
        let primes: Vec<usize> = primes().take(10).collect();
        assert_eq!(primes, [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]);
    }
}

/// We are goint to implement this thing in Rust
///
/// ```Haskell
/// primes = filterPrime [2..]
///     where filterPrime (p:xs) = p : filterPrime [x | x <- xs, x `mod` p /= 0]
/// ```

pub struct FilterPrime {
    input: Option<Box<dyn Iterator<Item = usize>>>,
    numbers: Option<Box<dyn Iterator<Item = usize>>>,
}

impl Iterator for FilterPrime {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        if self.numbers.is_some() {
            self.numbers.as_mut().unwrap().next()
        } else {
            let p = self.input.as_mut().unwrap().next().unwrap();
            let input = self.input.take().unwrap();
            self.numbers = Some(Box::new(FilterPrime {
                input: Some(Box::new(input.filter(move |&x| x % p != 0))),
                numbers: None,
            }));
            Some(p)
        }
    }
}

pub fn primes() -> FilterPrime {
    FilterPrime {
        input: Some(Box::new(2..)),
        numbers: None,
    }
}
